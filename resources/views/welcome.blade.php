<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            td{
                width: 150px;
            }
            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="">
            <div class="content">
                <div class="">

                    <table border="0.5">
                        <?php
                        $i=1;
                        ?>
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>language</th>
                            <th>year</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach( $books as $book)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $book -> id  }}</td>
                                <td>{{ $book -> name}}</td>
                                <td>{{ $book -> author}}</td>
                                <td>{{ $book -> language}}</td>
                                <td>{{ $book -> year}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>




                </div>
            </div>
        </div>
    </body>
</html>
