-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2016 at 06:54 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `author`) VALUES
(1, 'Akash', 'nkjbncxkjwdkwdnkdcndklwkld dknekldnlkewdd jkehbdkendf dkhekdc ekhbiyd dklehdie edkhei edkhnei ekdfhei efdjkhe edjkkhekd edjkhke ekdhbw ', 'Akram'),
(2, 'Akash', 'nkjbncxkjwdkwdnkdcndklwkld dknekldnlkewdd jkehbdkendf dkhekdc ekhbiyd dklehdie edkhei edkhnei ekdfhei efdjkhe edjkkhekd edjkhke ekdhbw ', 'Akram'),
(3, 'Barid', 'uuguewf kjnbhf kjbf kjnbf nkjbncxkjwdkwdnkdcndklwkld dknekldnlkewdd jkehbdkendf dkhekdc ekhbiyd dklehdie edkhei edkhnei ekdfhei efdjkhe edjkkhekd edjkhke ekdhbw ', 'Akram'),
(4, 'Sunmun', 'kh ihhf4 4ih4 jjijhlijijjjnkjbncxkjwdkwdnkdcndklwkld dknekldnlkewdd jkehbdkendf dkhekdc ekhbiyd dklehdie edkhei edkhnei ekdfhei efdjkhe edjkkhekd edjkhke ekdhbw ', 'Akram'),
(5, 'Sibly', 'lojoi oiijoier oijr3 ijij nkjbncxkjwdkwdnkdcndklwkld dknekldnlkewdd jkehbdkendf dkhekdc ekhbiyd dklehdie edkhei edkhnei ekdfhei efdjkhe edjkkhekd edjkhke ekdhbw ', 'Akram'),
(6, 'Ismaily', 'kjgh kjh lkn lkn kln nkjbncxkjwdkwdnkdcndklwkld dknekldnlkewdd jkehbdkendf dkhekdc ekhbiyd dklehdie edkhei edkhnei ekdfhei efdjkhe edjkkhekd edjkhke ekdhbw ', 'Akram');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `isbn` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `isbn`, `author`, `image`, `title`, `publisher`, `created_at`, `updated_at`) VALUES
(2, 'Akash', ';elfm;lqef,', 'efwef', 'rfre', 'wfewrf', '2016-03-14 17:52:02', '2016-03-14 11:52:02'),
(5, 'eeeeeeeeeeeeee', 'ssssssssss', 'oiiiiiiiiiiiii', 'akash', 'akash', '2016-03-14 17:51:49', '0000-00-00 00:00:00'),
(6, 'eeeeeeeeeeeeee', 'ssssssssss', 'oiiiiiiiiiiiii', 'akash', 'akash', '2016-03-14 17:51:49', '0000-00-00 00:00:00'),
(7, 'eeeeeeeeeeeeee', 'ssssssssss', 'oiiiiiiiiiiiii', 'akash', 'akash', '2016-03-14 17:51:49', '0000-00-00 00:00:00'),
(8, 'eeeeeeeeeeeeee', 'ssssssssss', 'oiiiiiiiiiiiii', 'akash', 'akash', '2016-03-14 17:51:49', '0000-00-00 00:00:00'),
(9, 'eeeeeeeeeeeeee', 'ssssssssss', 'oiiiiiiiiiiiii', 'akash', 'akash', '2016-03-14 17:51:49', '0000-00-00 00:00:00'),
(10, 'eeeeeeeeeeeeee', 'ssssssssss', 'oiiiiiiiiiiiii', 'akash', 'akash', '2016-03-14 17:51:49', '0000-00-00 00:00:00'),
(11, 'eeeeeeeeeeeeee', 'ssssssssss', 'oiiiiiiiiiiiii', 'akash', 'akash', '2016-03-14 17:51:49', '0000-00-00 00:00:00'),
(12, '1', '', '', '', '', '2016-03-14 11:53:12', '2016-03-14 11:53:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_12_144123_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
